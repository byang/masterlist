#!/usr/bin/python3

import datetime
import re
import subprocess
import sys
import threading

import cymruwhois
import debian.deb822
import dns.resolver
import ipaddress
import requests


try:
   resolve = dns.resolver.resolve
except AttributeError:
   resolve = dns.resolver.query

HEAD = '''Hi,

I was checking some things in the Debian mirror universe and noticed
a problem with your mirror:

'''

ERROR_MESSAGES = {
    'tracefile': '''\
o I notice there is no trace file matching your site name {mirrorname} in
  http://{mirrorname}/debian/project/trace/

  Please use our ftpsync script to mirror Debian.

  It should produce the trace files we require, and do the mirroring in a way
  that ensures the mirror is in a consistent state even during updates.

  http://{mirrorname}/debian/project/ftpsync/ftpsync-current.tar.gz
''',
    'tracedateformat': '''\
o The first line of the trace file at
  http://{mirrorname}/debian/project/trace/{mirrorname}
  should be the date, formatted like: {now}
''',
    'traceold': '''\
o It seems your mirror is not up to date, having last updated on {tracedate}

o The latest ftpsync versions come with a wrapper script called
  ftpsync-cron, which is intended to be run out of cron every hour or so
  (at a randomish offset).  The script monitors your upstream mirror,
  and if there was an update triggers a full sync using ftpsync.
  You might want to give it a try.

  NOTE: This requires having your upstream mirror name set correctly to
  a specific mirror which is also correctly configured (and thus has a
  trace file in /debian/project/trace with its name).
''',
    'duplicate-key': '''\
o The trace file at
  http://{mirrorname}/debian/project/trace/{mirrorname}
  contains the same key twice.
''',
    'traceinfo': '''\
o The trace file at
  http://{mirrorname}/debian/project/trace/{mirrorname}
  is missing some required information.

  We expect at least the Maintainer and Upstream-mirror values to be filled in,
  and your trace file is missing one or both of them.
''',
    'upstream-alias': '''\
o We recommend mirrors not sync directly from service aliases such as
  ftp.<CC>.debian.org (only HTTP is guaranteed to be available at
  ftp.<CC> sites).  Maybe change your config to sync from
  the site currently backing the ftp.<CC>.debian.org service you sync
  from?
''',
    'ftpsync-version': '''\
o The trace file
  http://{mirrorname}/debian/project/trace/{mirrorname}
  suggests that the ftpsync version you are using is very old.  Please upgrade.

  Using a modern ftpsync ensures updates are done in the correct order
  so APT clients don't get confused.   In particular, it processes
  translations, contents, and more files that have been added to the
  archive in recent years in the correct stage.  It should also produce
  trace files that contain more information that is useful for us and helps
  downstream mirrors sync better.

  http://{mirrorname}/debian/project/ftpsync/ftpsync-current.tar.gz
''',
    'not-ftpsync': '''\
o The trace file
  http://{mirrorname}/debian/project/trace/{mirrorname}
  suggests that you are not using ftpsync.

  Please use our ftpsync script to mirror Debian.

  It should produce better trace files, and do the mirroring in a way that
  ensures the mirror is in a consistent state even during updates.

  http://{mirrorname}/debian/project/ftpsync/ftpsync-current.tar.gz
''',
    'missing-source': '''\
o Your mirror appears not to carry source packages.

  To comply with the licenses of various pieces of software that you
  distribute on that mirror, you will probably need to also include
  sources.

  To be listed as a Debian mirror in our mirror list you MUST include
  sources, e.g. by adding "source" as an included architecture in ftpsync.conf.
''',
    'single-nameserver': '''\
o The DNS zone for {mirrorname} has a single name server.  At least two are
  required for better reliability/availability.
''',
    'nameservers-in-same-24-subnet': '''\
o The nameservers for {mirrorname} are all in the same /24 network.  For
  reliability we recommend having nameservers in more than one location.
''',
    'nameservers-in-same-AS': '''\
o The nameservers for {mirrorname} are all in the same AS.  For reliability we
  recommend having nameservers in more than one location.
''',
    'nameservers-error': '''\
o We couldn't check the nameservers for {mirrorname}.
''',
    'hosting-pbl': '''\
o The IP address of {mirrorname} appears to be dynamic or residential.
''',
    'hosting-error': '''\
o We couldn't check the hosting networking for {mirrorname}.
''',
}


def check_mirror_nameservers(mirrorname):
    try:
        zone = dns.resolver.zone_for_name(mirrorname)
        ns_answer = resolve(zone, 'NS')
        ips = [ip.address
               for ns in ns_answer.rrset.items
               for ip in resolve(ns.target, 'A').rrset.items]
        if len(set(ips)) == 1:
            return 'single-nameserver'
        network = ipaddress.IPv4Network((ips[0], 24), strict=False)
        if all(ipaddress.IPv4Network((ip, 24), strict=False) == network
               for ip in ips[1:]):
            return 'nameservers-in-same-24-subnet'
        c = cymruwhois.Client()
        asns = set(c.lookup(ip).asn for ip in ips)
        if len(asns) == 1:
            return 'nameservers-in-same-AS'
    except Exception:
        return 'nameservers-error'


def check_mirror_hosting(mirrorname):
    try:
        addresses = resolve(mirrorname, 'A')
        rev_addresses = [".".join(reversed(ip.to_text().split(".")))
                         for ip in addresses.rrset.items]
        try:
            pbl_results = [result.address
                           for ip in rev_addresses
                           for result in resolve('{0}.pbl.spamhaus.org.'.format(ip),
                                                 'A')]
        except dns.resolver.NXDOMAIN:
            pbl_results = []
        except Exception:
            return 'hosting-error'
        if any(result in ['127.0.0.10', '127.0.0.11']
               for result in pbl_results):
            return 'hosting-pbl'
    except Exception:
        return 'hosting-error'


def check_source(mirrorname):
    sourceindex = requests.get(
        'http://{0}/debian/dists/stable/main/source/Sources.xz'
        .format(mirrorname), timeout=5)
    if sourceindex.status_code != 200:
        return False
    xzcat = subprocess.Popen(['xzcat'],
                             stdin=subprocess.PIPE, stdout=subprocess.PIPE)

    def target():
        try:
            xzcat.stdin.write(sourceindex.content)
        except Exception:
            # we'll get a "broken pipe" error when xzcat is killed; don't spew
            # a traceback on stderr
            pass
    t = threading.Thread(target=target)
    t.start()
    source = next(debian.deb822.Sources.iter_paragraphs(xzcat.stdout))
    xzcat.terminate()
    t.join()
    package = requests.head('http://{0}/debian/{1}/{2}'.format(
        mirrorname, source['Directory'], source['Files'][0]['name']),
        timeout=5, allow_redirects=True)
    if package.status_code != 200:
        return False
    return True


def check_mirror(mirrorname):
    tracefile = requests.get('http://{0}/debian/project/trace/{0}'
                             .format(mirrorname), timeout=5)
    errors = []
    if tracefile.status_code != 200:
        return 'N/A', None, ['tracefile']
    traceinfo = tracefile.text.splitlines()
    try:
        ts = datetime.datetime.strptime(traceinfo[0],
                                        "%a %b %d %H:%M:%S %Z %Y")
    except ValueError:
        errors.append('tracedateformat')
        ts = None

    if ts is not None and datetime.datetime.utcnow() - ts > datetime.timedelta(hours=12):
        errors.append('traceold')
    tracedata = {}
    for line in traceinfo[1:]:
        try:
            key, value = line.split(': ', 1)
        except ValueError:
            errors.append('not-ftpsync')
            continue
        if key in tracedata:
            errors.append('duplicate-key')
        else:
            tracedata[key] = value
    if set(('Maintainer', 'Upstream-mirror')) - set(tracedata):
        errors.append('traceinfo')
    upstream_mirror = tracedata.get('Upstream-mirror', None)
    if upstream_mirror is not None and re.match(r'ftp[0-9]?\.[^.]+\.debian.org$', upstream_mirror):
        errors.append('upstream-alias')
    if 'Used ftpsync version' not in tracedata:
        if 'not-ftpsync' not in errors:
            errors.append('not-ftpsync')
    elif tracedata['Used ftpsync version'] < '20180513':
        errors.append('ftpsync-version')
    if not check_source(mirrorname):
        errors.append('missing-source')
    ns = check_mirror_nameservers(mirrorname)
    if ns is not None:
        errors.append(ns)
    hosting = check_mirror_hosting(mirrorname)
    if hosting is not None:
        errors.append(hosting)
    return tracefile.text, tracedata, errors


def main():
    mirrorname = sys.argv[1]
    tracefile, data, errors = check_mirror(mirrorname)
    if errors:
        formatstr = HEAD + '\n'.join(ERROR_MESSAGES[e] for e in errors)
        now = datetime.datetime.utcnow()
        print(formatstr.format(mirrorname=mirrorname,
                               tracedate=data.get('Date') if data is not None else 'unknown',
                               now=now.strftime('%a %b %d %H:%M:%S UTC %Y')))
    else:
        print('all good')
        print()
    print('Trace file:')
    print(tracefile)


if __name__ == '__main__':
    main()
